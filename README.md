<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta content="IE=10" http-equiv="X-UA-Compatible" />
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" />
    <link href="css/bootstrap.css" rel="stylesheet" />
    <link href="css/geral.css" rel="stylesheet" />
    <link href="css/form-controle.css" rel="stylesheet" />
    <link rel="stylesheet" href="css/style.css">

    <title>SEDUC - Matrícula</title>

    <style>
        .alert-warning {
            color: black;
        }
    </style>

    <!-- Google analytics -->
    <script>
        (function (i, s, o, g, r, a, m) {
            i['GoogleAnalyticsObject'] = r; i[r] = i[r] || function () {
                (i[r].q = i[r].q || []).push(arguments)
            }, i[r].l = 1 * new Date(); a = s.createElement(o),
                m = s.getElementsByTagName(o)[0]; a.async = 1; a.src = g; m.parentNode.insertBefore(a, m)
        })(window, document, 'script', '//www.google-analytics.com/analytics.js', 'ga');

        ga('create', 'UA-47765326-8', 'auto');
        ga('send', 'pageview');
    </script>
</head>
<body>
    <input type="hidden" id="id" value="" />
    <input type="hidden" id="turno" value="" />
    <input type="hidden" id="serie" value="" />
    <input type="hidden" id="matricula" value="" />

    <header>
        <div class="row">
            <div class="logos-header container">
                <div class="col-lg-12">
                    <!--<img src="img/logo-goias-login.png" alt="Inova Goiás" class="logo-inova img-responsive">-->
                    <img src="img/seduc-governo.jpg" alt="Governo de Goiás - Secretaria de Estado da Educação" class="logo-seduce img-responsive">
                </div>
            </div>
            <div class="titulo-matricula" style="margin-bottom: 7px;">
                <div class="container">
                    <div class="row">
                        <div class="col-lg-10 col-md-10 col-sm-8 col-xs-7">
                            <h1>Matrícula 2020/1</h1>
                        </div>
                        <div class="col-lg-2 col-md-2 col-sm-4 col-xs-5 inicio">
                            <a href="Index.html" class="active"><span class="glyphicon glyphicon-circle-arrow-left"></span>Início</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </header>

    <div class="container box-msg">
        <div class="row">
            <div class="col-md-12">
                <div class="alert alert-warning alert-dismissible fade in" role="alert">
                    <!--<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>-->
                    <i> * Campos obrigatórios! </i>
                </div>
            </div>
        </div>
    </div>

    <div class="container">
        <div class="box-categorias dados-pessoais">
            <!-- Aluno -->
            <div class="row">
                <div class="row marginb10px">
                    <div class="passo-circulo"> <h3> 1 </h3> </div>
                    <div class="passo-titulo"> <h3> DADOS PESSOAIS DO ALUNO </h3></div>
                </div>
                <div class="col-md-12">
                    <div class="form-group">
                        <label for="Aluno"><span class="obrigatorio">*</span>Nome do aluno</label>
                        <input id="Aluno" type="text" name="nome" class="form-control" aria-describedby="helpAluno" maxlength="80" />
                        <span data-campo="Aluno" class="help-block"></span>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-4">
                    <div class="form-group">
                        <label for="DataNascimento"><span class="obrigatorio">*</span>Data de nascimento</label>
                        <input id="DataNascimento" type="text" name="nome" class="form-control DataNascimento" />
                        <span id="helpDataNascimento" data-campo="DataNascimento" class="help-block"></span>
                    </div>
                </div>

                <div class="col-md-4">
                    <div class="form-group">
                        <label><span class="obrigatorio">*</span>Sexo</label>
                        <div class="row">
                            <div class="marcar-sexo">
                                <div class="col-md-4">
                                    <div class="radio">
                                        <label>
                                            <input type="radio" name="sexo" value="M" id="sex_m" class="sexo">
                                            Masculino
                                        </label>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="radio">
                                        <label>
                                            <input type="radio" name="sexo" value="F" id="sex_f" class="sexo" checked>
                                            Feminino
                                        </label>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-md-4">
                    <div class="form-group">
                        <label for="ddlCor"><span class="obrigatorio">*</span>Cor / Raça</label>
                        <select class="form-control" id="ddlCor">
                            <option value="0">Selecione</option>
                            <option value="1">Não declarada</option>
                            <option value="2">Branca</option>
                            <option value="3">Preta</option>
                            <option value="4">Parda</option>
                            <option value="5">Amarela</option>
                            <option value="6">Indígena</option>
                        </select>
                        <span data-campo="ddlCor" class="help-block"></span>
                    </div>
                </div>


            </div>

            <!-- Filiação 1 -->
            <div class="row">
                <div class="col-md-12">
                    <div class="form-group">
                        <label for="Mae"><span class="obrigatorio">*</span>Nome da mãe</label><span></span>
                        <input id="Mae" type="text" name="nome" class="form-control" maxlength="80" placeholder="" />
                        <span data-campo="Mae" class="help-block"></span>
                    </div>
                </div>
            </div>

            <!-- Filiação 2 -->
            <div class="row">
                <div class="col-md-12">
                    <div class="form-group">
                        <label for="pai">Nome do pai</label>
                        <input id="pai" type="text" name="nome" class="form-control filiacao2" maxlength="80" placeholder="" />
                        <span data-campo="pai" class="help-block"></span>
                    </div>
                </div>
            </div>

            <!-- Responsável -->
            <div class="row">
                <div class="col-md-12">
                    <div class="form-group">
                        <label for="Responsavel"><span class="obrigatorio">*</span>Nome do responsável</label>
                        <input id="Responsavel" type="text" name="nome" class="form-control" maxlength="80" />
                        <span data-campo="Responsavel" class="help-block"></span>
                    </div>
                </div>
            </div>

            <!-- CPF, CPF de quem? -->
            <div class="row">
                <div class="col-md-6">
                    <div class="form-group">
                        <label for="CPF"><span class="obrigatorio">*</span>CPF</label>
                        <input id="CPF" type="text" name="nome" class="form-control" />
                        <span data-campo="CPF" class="help-block"></span>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        <label for="respCPF"><span class="obrigatorio">*</span>CPF de quem?</label>
                        <select class="form-control" id="respCPF">
                            <option value="0">Selecione</option>
                            <option value="A">Aluno</option>
                            <option value="R">Responsável</option>
                        </select>
                        <span data-campo="respCPF" class="help-block"></span>
                    </div>
                </div>
            </div>

            <!-- DEFICIÊNCIA -->

            <div class="row">


                <div class="col-md-12">
                    <div class="form-group">
                        <label for="">
                            <span class="obrigatorio">*</span>
                            Deficiência, transtorno do espectro autista ou altas habilidades/superdotação
                        </label>
                        <div class="row">
                            <div class="marcar-deficiencia">
                                <div class="col-md-1">
                                    <div class="radio">
                                        <label>
                                            <input id="optDSim" type="radio" name="deficiencia" value="Sim" class="r-deficiencia">
                                            Sim
                                        </label>
                                    </div>
                                </div>
                                <div class="col-md-11">
                                    <div class="radio">
                                        <label>
                                            <input id="optDNao" type="radio" name="deficiencia" value="Nao" class="option-false r-deficiencia">
                                            Não
                                        </label>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-md-12">
                    <div class="alert alert-danger text-justify aviso" role="alert">
                        Alunos com deficiência, transtorno do espectro autista ou altas habilidades/superdotação deverão apresentar Laudo Médico na efetivação da Matrícula.
                    </div>
                </div>

                <div class="col-md-12">
                    <div class="alert alert-warning text-justify aviso" role="alert">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label for="">
                                        Deficiência
                                    </label>

                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="checkbox d-deficiencia">

                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label for="">
                                        Transtorno global do desenvolvimento
                                    </label>

                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="checkbox d-transtorno">

                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label for="">
                                        Altas habilidades/Superdotação
                                    </label>

                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="checkbox d-habilidades">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- FIM DEFICIÊNCIA -->
                </div>





            </div>
        </div>
        <div class="box-categorias">
            <!-- Data de nascimento, Telefone contato, Celular pai, Celular mãe, Celular responsável -->
            <div class="row">
                <div class="row marginb10px">
                    <div class="passo-circulo"> <h3> 2 </h3> </div>
                    <div class="passo-titulo"> <h3> CONTATO </h3></div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        <label for="TelefoneContato"><span class="obrigatorio">*</span>Telefone de contato</label>
                        <input id="TelefoneContato" type="text" name="nome" class="form-control" />
                        <span data-campo="TelefoneContato" class="help-block"></span>
                    </div>
                </div>

                <!--<div class="col-md-3">
                    <div class="form-group">
                        <label for="FoneMae">Celular da mãe</label>
                        <input id="FoneMae" type="text" name="nome" class="form-control" />
                        <span data-campo="FoneMae" class="help-block"></span>
                    </div>
                </div>

                <div class="col-md-3">
                    <div class="form-group">
                        <label for="FonePai">Celular do pai</label>
                        <input id="FonePai" type="text" name="nome" class="form-control" />
                        <span data-campo="FonePai" class="help-block"></span>
                    </div>
                </div>-->

                <div class="col-md-6">
                    <div class="form-group">
                        <label for="FoneResponsavel">Celular do responsável</label>
                        <input id="FoneResponsavel" type="text" name="nome" class="form-control" />
                        <span data-campo="FoneResponsavel" class="help-block"></span>
                    </div>
                </div>
            </div>

            <!-- E-mail do aluno -->
            <div class="row">
                <div class="col-md-12">
                    <div class="form-group">
                        <label for="Email"><span class="obrigatorio">*</span>E-mail</label>
                        <input type="email" name="nome" class="form-control" id="Email" />
                        <span data-campo="Email" class="help-block"></span>
                    </div>
                </div>
            </div>
        </div>

        <div class="box-categorias">
            <div class="row">
                <div class="row marginb10px">
                    <div class="passo-circulo"> <h3> 3 </h3> </div>
                    <div class="passo-titulo"> <h3> ENDEREÇO </h3></div>
                </div>
                <div class="col-md-12">
                    <div class="form-group">
                        <label><span class="obrigatorio">*</span>Localização</label>
                        <div class="row">
                            <div class="marcar-localizacao">
                                <div class="col-md-1">
                                    <div class="radio">
                                        <label>
                                            <input id="optR" type="radio" name="localizacao" value="R" class="localizacao">
                                            Rural
                                        </label>
                                    </div>
                                </div>
                                <div class="col-md-11">
                                    <div class="radio">
                                        <label>
                                            <input id="optU" type="radio" name="localizacao" value="U" class="option-false localizacao">
                                            Urbana
                                        </label>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-md-3">
                    <div class="form-group">
                        <label for="CEP"><span class="obrigatorio">*</span>CEP</label>
                        <input id="CEP" type="text" name="nome" class="form-control" />
                        <span data-campo="CEP" class="help-block"></span>
                    </div>
                </div>

                <div class="col-md-9">
                    <div class="form-group">
                        <label for="Endereco"><span class="obrigatorio">*</span>Endereço</label>
                        <input id="Endereco" type="text" name="nome" class="form-control" maxlength="200" />
                        <span data-campo="Endereco" class="help-block"></span>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-md-2">
                    <div class="form-group">
                        <label for="Numero">Número</label>
                        <input type="text" name="nome" class="form-control" id="Numero" />
                    </div>
                </div>

                <div class="col-md-7">
                    <div class="form-group">
                        <label for="Complemento">Complemento</label>
                        <input type="text" name="nome" class="form-control" id="Complemento" maxlength="200" />
                    </div>
                </div>

                <div class="col-md-3">
                    <div class="form-group">
                        <label for="Bairro"><span class="obrigatorio">*</span>Bairro</label>
                        <input id="Bairro" type="text" name="nome" class="form-control" maxlength="200" />
                        <span data-campo="Bairro" class="help-block"></span>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-md-2">
                    <div class="form-group">
                        <label for="UF"><span class="obrigatorio">*</span>UF</label>
                        <input id="UF" type="text" name="nome" class="form-control" />
                        <span data-campo="UF" class="help-block"></span>
                    </div>
                </div>

                <div class="col-md-10">
                    <div class="form-group">
                        <label for="Municipio"><span class="obrigatorio">*</span>Município</label>
                        <input id="Municipio" type="text" name="nome" class="form-control" />
                        <span data-campo="Municipio" class="help-block"></span>
                    </div>
                </div>
            </div>
        </div>
        <div class="box-categorias">
            <!--Curso,  Série, Turno -->
            <div class="row">
                <div class="row marginb10px">
                    <div class="passo-circulo"> <h3> 4 </h3> </div>
                    <div class="passo-titulo"> <h3> CURSO </h3> </div>
                </div>
                <div class="col-md-3">
                    <div class="radio">
                        <label>
                            <input type="radio" class="tipo-ensino" id="optFundamental" name="modalidade" value="2">
                            Ensino fundamental
                        </label>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="radio">
                        <label>
                            <input type="radio" class="tipo-ensino" id="optMedio" name="modalidade" value="3" checked>
                            Ensino médio
                        </label>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="radio">
                        <label>
                            <input type="radio" class="tipo-ensino" id="optEJA" name="modalidade" value="4">
                            EJA
                        </label>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="radio">
                        <label>
                            <input type="radio" class="tipo-ensino" id="optEJATEC" name="modalidade" value="5">
                            EJA TEC <span class="info-modalidade">(a distância)</span>
                        </label>
                    </div>
                </div>

            </div>
            <div class="row">
                <div class="col-md-4">
                    <div class="form-group opcoes">
                        <label for="NomePai">Curso</label>
                        <select class="form-control" id="Curso">
                            <option value="0">Selecione</option>
                        </select>
                        <span id="helpCurso" data-campo="Curso" class="help-block"></span>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="form-group opcoes">
                        <label for="NomePai">Série</label>
                        <select class="form-control" id="ddlSerie">
                            <option value="0">Selecione</option>
                        </select>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="form-group opcoes">
                        <label for="">Turno</label>
                        <select class="form-control" id="ddlTurno">
                            <option value="0">Selecione</option>
                            <option value="1">Matutino</option>
                            <option value="2">Vespertino</option>
                            <option value="3">Noturno</option>
							<opition value="4">Tempo integral</opition>
                            <option value="5">Intermediário</option>
                        </select>
                        <span id="helpTurno" data-campo="ddlTurno" class="help-block"></span>
                    </div>
                </div>
            </div>
        </div>
        <!-- 1º Opção -->
        <div class="box-categorias">
            <div class="row">
                <div class="row marginb10px">
                    <div class="passo-circulo"> <h3> 5 </h3> </div>
                    <div class="passo-titulo"> <h3> ESCOLAS </h3></div>
                </div>
                <div class="col-md-12">
                    <div class="alert alert-danger text-justify aviso" role="alert">
                        <p>
                            <strong>Atenção! </strong>
                        </p>
                        <p>
                            É importante informar as três opções de escola por ordem de prioridade. Caso não haja vaga na primeira opção o aluno será alocado na segunda opção, terceira opção ou, em último caso, em uma escola mais próxima à primeira opção.
                        </p>

                        <p>
                            Existem escolas com o mesmo nome em municípios diferentes. Ao consultar a escola, o município será destacado de verde para auxiliar o aluno/responsável.
                        </p>
                    </div>
                </div>
                <div class="col-md-12">
                    <div class="form-group opcoes">
                        <label for="Opcao1"><strong> <span class="obrigatorio">*</span>1ª OPÇÃO </strong></label>
                        <input id="Opcao1" type="text" name="nome" class="form-control" placeholder="Digite o nome da escola" />
                        <span data-campo="Opcao1" class="help-block validcao-escola"></span>
                    </div>
                </div>
            </div>
            <hr />
            <!-- 2º Opção -->
            <div class="row">
                <div class="col-md-12">
                    <div class="form-group opcoes">
                        <label for="Opcao2"><strong> 2ª OPÇÃO </strong> </label>
                        <input type="text" name="nome" class="form-control" id="Opcao2" placeholder="Digite o nome da escola" />
                        <span data-campo="Opcao2" class="help-block validcao-escola"></span>
                    </div>
                </div>
            </div>
            <hr />
            <!-- 3º Opção -->
            <div class="row">
                <div class="col-md-12">
                    <div class="form-group opcoes">
                        <label for="Opcao3"><strong> 3ª OPÇÃO </strong></label>
                        <input type="text" name="nome" class="form-control" id="Opcao3" placeholder="Digite o nome da escola" />
                        <span data-campo="Opcao3" class="help-block validcao-escola"></span>
                    </div>
                </div>
            </div>
        </div>


        <div class="box-categorias">
            <div class="row">
                <div class="row marginb10px">
                    <div class="passo-circulo"> <h3> 6 </h3> </div>
                    <div class="passo-titulo"> <h3> AUTORIZAÇÃO DE USO DA IMAGEM E VOZ </h3></div>
                </div>

                <div class="col-md-12">
                    <div class="form-group">
                        * <strong>AUTORIZO</strong> o uso da imagem e voz, em todo e qualquer material fotográfico realizado pela Secretaria de Estado da Educação de Goiás (Seduc) de acordo com a <a href="https://sige.educacao.go.gov.br/sige/modulos/academico/DocumentosGerais/Port_4556_2019_SEDUC.pdf" target="_blank"> Portaria nº 4556/2019</a>. A assinatura do termo será coletada na efetivação da matrícula.
                    </div>
                </div>

                <div class="col-md-4">
                    <div class="form-group">
                        <div class="row">
                            <div class="marcar-autorizo">
                                <div class="col-md-4">
                                    <div class="radio">
                                        <label>
                                            <input type="radio" name="autorizo" value="true" id="auto_true" class="autorizo" checked>
                                            Sim
                                        </label>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="radio">
                                        <label>
                                            <input type="radio" name="autorizo" value="false" id="auto_false" class="autorizo" checked>
                                            Não
                                        </label>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>




            </div>

        </div>


        <!--<div class="box-categorias rede">

            <div class="row">
                <div class="row marginb10px">
                    <div class="passo-circulo"> <h3> 6 </h3> </div>
                    <div class="passo-titulo"> <h3> A ESCOLA QUE O ALUNO ESTUDOU ANTERIORMENTE PERTENCE A </h3> </div>
                </div>
                <div class="marcar-rede">
                    <div class="col-md-3">
                        <div class="radio">
                            <div class="form-group">
                                <label>
                                    <input type="radio" id="redePublica" name="tipo-rede" class="tipo-rede" value="1" required />Rede Pública
                                </label>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-9">
                        <div class="radio">
                            <div class="form-group">
                                <label>
                                    <input type="radio" id="redePrivada" name="tipo-rede" class="tipo-rede" value="2" required />Rede Privada
                                </label>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>-->
        <!--<div class="row btn-ajuste marginb20px">
            <div class="form-group checkbox">
                <label>
                    <input type="checkbox" id="solicitaEmail" checked> Solicito a criação de e-mail para o aluno.
                </label>
            </div>
        </div>-->
        <br />
        <div class="row">
            <div class="col-md-12">
                <div class="alert alert-success aviso" role="alert">
                    <p> Ao solicitar a matrícula, guarde bem o número da solicitação. Você precisará dele para saber a escola que o aluno irá estudar!</p>
                </div>
            </div>
            <div class="row btn-ajuste marginb20px">
                <input id="btnSalvar" type="button" name="btnSalvar" value="Salvar" class="btn btn-large btn-success" />
            </div>
        </div>
    </div>
    <nav class="navbar navbar-default navbar-fixed-bottom">
        <div class="links-gerais">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <div class="link-ref text-center">
                            <a href="Contato.html" class="link-na-inscricao"><span class="setas glyphicon glyphicon-envelope"></span> Dúvidas ou problemas com a solicitação? </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </nav>

    <div id="modal" class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
                    <h4 class="modal-title" id="myLargeModalLabel">
                        <label id="lblTitulo">Atenção</label>
                    </h4>
                </div>
                <div class="modal-body">
                    <div id="painel" class="panel">
                        <div id="corpoPainel" class="panel-body">
                            <label class="msg"></label>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Fechar</button>
                    <!--<button type="button" class="btn btn-primary confirma">Save changes</button>-->
                </div>
            </div>
        </div>
    </div>

    <script src="js/jquery-2.1.3.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <!--<script src="js/meiomask.js"></script>-->
    <script src="js/jquery.mask.min.js"></script>
    <script src="js/inscricao.js?04"></script>
    <script src="js/jquery.autocomplete.min.js"></script>
    <script src="js/jquery.blockUI.js"></script>
    <script src="js/bootbox.min.js"></script>

    <script type="text/javascript">
        $(document).ready(function () {
            inscricao.inicialize();
        });
    </script>
</body>
</html>